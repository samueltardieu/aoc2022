fn solve(input: &str, f: impl Fn(u32, u32) -> u32) -> u32 {
    input
        .lines()
        .map(|s| {
            let [s0, 9, s1] = s.bytes().map(|b| u32::from(b) % 23 % 19).collect::<Vec<_>>()[..] else { unreachable!() };
            f(s0, s1)
        })
        .sum()
}

#[aoc(day2, part1)]
fn part1(input: &str) -> u32 {
    solve(input, |s0, s1| s1 + 1 + ((4 + s1 - s0) % 3) * 3)
}

#[aoc(day2, part2)]
fn part2(input: &str) -> u32 {
    solve(input, |s0, s1| (s0 + s1 + 2) % 3 + 1 + s1 * 3)
}
