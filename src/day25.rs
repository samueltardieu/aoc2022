fn read_snafu(s: &str) -> i64 {
    s.bytes().fold(0, |a, b| {
        a * 5
            + match b {
                b'0' => 0,
                b'1' => 1,
                b'2' => 2,
                b'-' => -1,
                b'=' => -2,
                _ => unreachable!(),
            }
    })
}

fn write_snafu(mut i: i64) -> String {
    let mut v = vec![];
    while i != 0 {
        let d = i % 5;
        v.push(b"012=-"[d as usize]);
        i = (i - if d <= 2 { d } else { d - 5 }) / 5;
    }
    v.into_iter().rev().map(|b| b as char).collect()
}

#[aoc(day25, part1)]
fn part1(input: &str) -> String {
    write_snafu(input.lines().map(read_snafu).sum())
}