use crate::intervals::Interval;
use itertools::Itertools;
use std::collections::HashSet;

type Pos = (isize, isize);
type Sensor = (Pos, Pos, usize);

fn parse(input: &str) -> Vec<Sensor> {
    let re = regex::Regex::new(r"-?\d+").unwrap();
    input.lines().map(|l| {
        let Some((sx, sy, bx, by)) = re.find_iter(l).map(|s| s.as_str().parse::<isize>().unwrap()).next_tuple() else { unreachable!() };
        ((sx, sy), (bx, by), sx.abs_diff(bx)+sy.abs_diff(by))
    }).collect()
}

fn no_beacon(y: isize, sensors: &[Sensor]) -> Vec<Interval<isize>> {
    let mut intervals = vec![];
    for &((sx, sy), _, d) in sensors {
        let i = d as isize - y.abs_diff(sy) as isize;
        if i >= 0 {
            Interval::new(sx - i, sx + i).insert_into(&mut intervals);
        }
    }
    intervals
}

#[aoc(day15, part1)]
fn part1(input: &str) -> usize {
    const Y: isize = 2_000_000;
    let sensors = parse(input);
    no_beacon(Y, &sensors)
        .into_iter()
        .map(|i| (*i.high() - *i.low()) as usize + 1)
        .sum::<usize>()
        - sensors
            .iter()
            .filter_map(|(_, (bx, by), _)| (*by == Y).then_some(bx))
            .collect::<HashSet<_>>()
            .len()
}

#[aoc(day15, part2)]
fn part2(input: &str) -> isize {
    let sensors = parse(input);
    let beacons = sensors.iter().map(|&(_, b, _)| b).collect::<HashSet<_>>();
    for y in 0..=4_000_000 {
        if let [i0, i1] = &*no_beacon(y, &sensors) {
            let x = i0.after();
            if x == i1.before() && (0..=4_000_000).contains(&x) && !beacons.contains(&(x, y)) {
                return x * 4_000_000 + y;
            }
        }
    }
    unreachable!()
}
