use pathfinding::prelude::topological_sort;
use rustc_hash::FxHashMap;

enum Monkey<'a> {
    Number(i64),
    Op(&'a str, u8, &'a str),
}

impl<'a> Monkey<'a> {
    fn depends(&self) -> Vec<&str> {
        match self {
            Monkey::Number(_) => vec![],
            Monkey::Op(a, _, b) => vec![a, b],
        }
    }

    fn eval(&self, values: &FxHashMap<&str, i64>) -> i64 {
        match self {
            Monkey::Number(x) => *x,
            Monkey::Op(a, b'+', b) => values[a] + values[b],
            Monkey::Op(a, b'*', b) => values[a] * values[b],
            Monkey::Op(a, b'/', b) => values[a] / values[b],
            Monkey::Op(a, _, b) => values[a] - values[b],
        }
    }
}

fn parse(input: &str) -> FxHashMap<&str, Monkey<'_>> {
    input
        .lines()
        .map(|l| {
            let (name, value) = l.split_once(": ").unwrap();
            let mut it = value.split(' ');
            let f = it.next().unwrap();
            (
                name,
                f.parse::<i64>().map_or_else(
                    |_| Monkey::Op(f, it.next().unwrap().as_bytes()[0], it.next().unwrap()),
                    Monkey::Number,
                ),
            )
        })
        .collect()
}

#[aoc(day21, part1)]
fn part1(input: &str) -> i64 {
    let monkeys = parse(input);
    let deps = topological_sort(&["root"], |n| monkeys[n].depends()).unwrap();
    let mut values = FxHashMap::default();
    for n in deps.into_iter().rev() {
        let r = monkeys[n].eval(&values);
        if n == "root" {
            return r;
        }
        values.insert(n, r);
    }
    unreachable!();
}

#[aoc(day21, part2)]
fn part2(input: &str) -> i64 {
    let mut monkeys = parse(input);
    let Monkey::Op(_, ref mut op, _) = monkeys.get_mut("root").unwrap() else { unreachable!()};
    *op = b'=';
    let deps = topological_sort(&["root"], |n| monkeys[n].depends()).unwrap();
    let mut values = FxHashMap::default();
    let mut skip = 0;
    for (s, m) in deps.iter().rev().enumerate() {
        if *m == "humn" {
            skip = s + 1;
            break;
        }
        let r = monkeys[m].eval(&values);
        values.insert(m, r);
    }
    let mut bit = 56;
    let mut h = 1 << bit;
    loop {
        values.insert("humn", h);
        for m in deps.iter().rev().skip(skip) {
            let r = monkeys[m].eval(&values);
            if *m == "root" {
                if r == 0 {
                    bit = -1;
                    h -= 1;
                    break;
                }
                if bit == -1 {
                    return h + 1;
                }
                if r < 0 {
                    h -= 1 << bit;
                    bit -= 1;
                }
                h += 1 << bit;
                break;
            }
            values.insert(m, r);
        }
    }
}
