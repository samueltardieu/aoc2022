use std::{hash::Hash, str::FromStr};

use pathfinding::prelude::bfs_reach;
use regex::Regex;

fn mineral_index(mineral: &str) -> usize {
    match mineral {
        "ore" => 0,
        "clay" => 1,
        "obsidian" => 2,
        _ => 3,
    }
}

#[derive(Debug, Default)]
struct Blueprint {
    index: usize,
    recipes: [[usize; 4]; 4],
}

lazy_static::lazy_static! {
    static ref LINE_RE: Regex = Regex::new(r"Blueprint (\d+): (.*)").unwrap();
    static ref ROBOT_RE: Regex = Regex::new(r"Each (.*?) robot costs (.*?)\.").unwrap();
    static ref RECIPE_RE: Regex = Regex::new(r"(\d+) (\S+)").unwrap();
}

impl FromStr for Blueprint {
    type Err = eyre::Report;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let m = LINE_RE.captures(s).unwrap();
        let mut bp = Blueprint {
            index: m[1].parse::<usize>().unwrap(),
            ..Blueprint::default()
        };
        for robot in ROBOT_RE.captures_iter(&m[2]) {
            let robot_index = mineral_index(&robot[1]);
            for recipe in RECIPE_RE.captures_iter(&robot[2]) {
                let n = recipe[1].parse::<usize>().unwrap();
                let i = mineral_index(&recipe[2]);
                bp.recipes[robot_index][i] = n;
            }
        }
        Ok(bp)
    }
}

#[derive(Clone, Debug, Eq)]
struct State {
    round: usize,
    robots: [usize; 4],
    content: [usize; 4],
}

impl PartialEq for State {
    fn eq(&self, other: &Self) -> bool {
        self.robots == other.robots && self.content == other.content
    }
}

impl Hash for State {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.robots.hash(state);
        self.content.hash(state);
    }
}

impl State {
    fn neighbours(&self, bp: &Blueprint, maxs: &mut Vec<usize>) -> Vec<State> {
        if maxs.len() > self.round {
            if maxs[self.round] > self.content[3] {
                return vec![];
            }
            maxs[self.round] = self.content[3];
        } else {
            maxs.push(self.content[3]);
        }
        let mut new_content = self.content;
        for (c, r) in new_content.iter_mut().zip(self.robots) {
            *c += r;
        }
        let mut states = vec![State {
            round: self.round + 1,
            robots: self.robots,
            content: new_content,
        }];
        for i in (0..4).rev() {
            if bp.recipes[i] != [0, 0, 0, 0] && (0..4).all(|m| self.content[m] >= bp.recipes[i][m])
            {
                let mut new_content = new_content;
                for (c, r) in new_content.iter_mut().zip(bp.recipes[i]) {
                    *c -= r;
                }
                let mut new_robots = self.robots;
                new_robots[i] += 1;
                states.push(State {
                    round: self.round + 1,
                    robots: new_robots,
                    content: new_content,
                });
                if i == 3 {
                    break;
                }
            }
        }
        states
    }
}

fn part1_geode(bp: &Blueprint) -> usize {
    let state = State {
        round: 0,
        robots: [1, 0, 0, 0],
        content: [0, 0, 0, 0],
    };
    let mut maxs = vec![];
    bp.index
        * bfs_reach(state, |s| {
            if s.round == 24 {
                vec![]
            } else {
                s.neighbours(bp, &mut maxs)
            }
        })
        .max_by_key(|s| s.content[3])
        .unwrap()
        .content[3]
}

#[aoc(day19, part1)]
fn part1(bps: &[Blueprint]) -> usize {
    bps.iter().map(part1_geode).sum()
}

fn part2_geode(bp: &Blueprint) -> usize {
    let state = State {
        round: 0,
        robots: [1, 0, 0, 0],
        content: [0, 0, 0, 0],
    };
    let mut maxs = vec![];
    bfs_reach(state, |s| {
        if s.round == 32 {
            vec![]
        } else {
            s.neighbours(bp, &mut maxs)
        }
    })
    .max_by_key(|s| s.content[3])
    .unwrap()
    .content[3]
}

#[aoc(day19, part2)]
fn part2(bps: &[Blueprint]) -> usize {
    bps.iter().take(3).map(part2_geode).product()
}
