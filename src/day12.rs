use pathfinding::prelude::{bfs, Matrix};

fn parse(input: &str) -> (Matrix<u8>, (usize, usize), (usize, usize)) {
    let mut m = Matrix::from_rows(input.lines().map(str::bytes)).unwrap();
    let s = m.items().find(|(_, &p)| p == b'S').unwrap().0;
    let e = m.items().find(|(_, &p)| p == b'E').unwrap().0;
    m[s] = b'a';
    m[e] = b'z';
    (m, s, e)
}

#[aoc(day12, part1)]
fn part1(input: &str) -> usize {
    let (ref m, s, e) = parse(input);
    bfs(
        &s,
        |&p| m.neighbours(p, false).filter(move |&q| m[q] <= m[p] + 1),
        |&p| p == e,
    )
    .unwrap()
    .len()
        - 1
}

#[aoc(day12, part2)]
fn part2(input: &str) -> usize {
    let (ref m, _, e) = parse(input);
    bfs(
        &e,
        |&p| m.neighbours(p, false).filter(move |&q| m[p] <= m[q] + 1),
        |&p| m[p] == b'a',
    )
    .unwrap()
    .len()
        - 1
}
