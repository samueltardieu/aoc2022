use crate::{FxHashMap, FxHashSet};
use itertools::{iproduct, Itertools};
use pathfinding::prelude::bfs_reach;

type Pos = (i32, i32, i32);

fn parse(input: &str) -> FxHashSet<Pos> {
    input
        .lines()
        .map(|s| sscanf::sscanf!(s, "{i32},{i32},{i32}").unwrap())
        .collect()
}

const SIDES: [Pos; 6] = [
    (-1, 0, 0),
    (1, 0, 0),
    (0, -1, 0),
    (0, 1, 0),
    (0, 0, -1),
    (0, 0, 1),
];

fn free_sides(cubes: &FxHashSet<Pos>) -> FxHashMap<Pos, usize> {
    let mut r = FxHashMap::default();
    iproduct!(cubes.iter().copied(), SIDES).for_each(|(p, d)| {
        let p = (p.0 + d.0, p.1 + d.1, p.2 + d.2);
        if !cubes.contains(&p) {
            *r.entry(p).or_default() += 1;
        }
    });
    r
}

#[aoc(day18, part1)]
fn part1(input: &str) -> usize {
    free_sides(&parse(input)).values().sum()
}

#[aoc(day18, part2)]
fn part2(input: &str) -> usize {
    let cubes = parse(input);
    let (x_min, x_max) = cubes.iter().map(|p| p.0).minmax().into_option().unwrap();
    let (y_min, y_max) = cubes.iter().map(|p| p.1).minmax().into_option().unwrap();
    let (z_min, z_max) = cubes.iter().map(|p| p.2).minmax().into_option().unwrap();
    let outside = bfs_reach((x_min - 1, y_min - 1, z_min - 1), |p| {
        SIDES
            .into_iter()
            .filter_map(|d| {
                let p = (p.0 + d.0, p.1 + d.1, p.2 + d.2);
                (!cubes.contains(&p)
                    && ((x_min - 1)..=(x_max + 1)).contains(&p.0)
                    && ((y_min - 1)..=(y_max + 1)).contains(&p.1)
                    && ((z_min - 1)..=(z_max + 1)).contains(&p.2))
                .then_some(p)
            })
            .collect::<Vec<_>>()
    })
    .collect::<Vec<_>>();
    let free = free_sides(&cubes);
    outside.into_iter().filter_map(|p| free.get(&p)).sum()
}
