use pathfinding::prelude::{directions::DIRECTIONS_4, Matrix};

#[aoc(day8, part1)]
fn part1(input: &[&[u8]]) -> usize {
    let m = Matrix::from_rows(input.iter().copied()).unwrap();
    m.keys()
        .filter(|&(r, c)| {
            let v = m[(r, c)];
            DIRECTIONS_4
                .into_iter()
                .any(|d| m.in_direction((r, c), d).all(|(rr, cc)| m[(rr, cc)] < v))
        })
        .count()
}

#[aoc(day8, part2)]
fn part2(input: &[&[u8]]) -> usize {
    let m = Matrix::from_rows(input.iter().copied()).unwrap();
    m.keys()
        .map(|(r, c)| {
            let v = m[(r, c)];
            DIRECTIONS_4
                .into_iter()
                .map(|d| {
                    m.in_direction((r, c), d)
                        .try_fold(0, |seen, (rr, cc)| {
                            if m[(rr, cc)] < v {
                                Ok(seen + 1)
                            } else {
                                Err(seen + 1)
                            }
                        })
                        .unwrap_or_else(std::convert::identity)
                })
                .product()
        })
        .max()
        .unwrap()
}
