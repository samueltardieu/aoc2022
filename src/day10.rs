use pathfinding::prelude::Grid;

fn parse(input: &str) -> impl Iterator<Item = i32> + '_ {
    input
        .lines()
        .scan(1, |x, s| {
            Some(if let Some(v) = s.strip_prefix("addx ") {
                let r = vec![*x, *x];
                *x += v.parse::<i32>().unwrap();
                r
            } else {
                vec![*x]
            })
        })
        .flatten()
}

#[aoc(day10, part1)]
fn part1(input: &str) -> i32 {
    parse(input)
        .enumerate()
        .skip(19)
        .step_by(40)
        .map(|(i, x)| x * (i + 1) as i32)
        .sum()
}

#[aoc(day10, part2)]
fn part2(input: &str) -> String {
    let screen = parse(input)
        .enumerate()
        .filter_map(|(i, x)| (x.abs_diff(i as i32 % 40) <= 1).then_some((i % 40, i / 40)))
        .collect::<Grid>();
    format!("{screen:#?}")
}
