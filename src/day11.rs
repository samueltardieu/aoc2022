use std::{cell::RefCell, num::ParseIntError, rc::Rc, str::FromStr};

fn b<T>(x: Option<T>) -> eyre::Result<T> {
    x.ok_or_else(|| eyre::eyre!("parse error"))
}

fn to_op(s: &str) -> eyre::Result<Rc<dyn Fn(u64) -> u64>> {
    let s = b(s.split_once("d "))?.1;
    match (s.as_bytes()[0], s.split_at(2).1.parse::<u64>()) {
        (b'+', Ok(n)) => Ok(Rc::new(move |x| x + n)),
        (_, Ok(n)) => Ok(Rc::new(move |x| x * n)),
        _ => Ok(Rc::new(|x| x * x)),
    }
}

struct Monkey {
    items: Vec<u64>,
    op: Rc<dyn Fn(u64) -> u64>,
    test: u64,
    t: usize,
    f: usize,
    examined: usize,
}

impl FromStr for Monkey {
    type Err = eyre::Report;

    fn from_str(str: &str) -> Result<Self, Self::Err> {
        fn int<'a, T: FromStr<Err = ParseIntError>>(
            lines: &mut impl Iterator<Item = &'a str>,
        ) -> eyre::Result<T> {
            Ok(b(b(lines.next())?.split_whitespace().last())?.parse::<T>()?)
        }
        let mut lines = str.lines();
        b(lines.next())?;
        let items = b(b(lines.next())?.strip_prefix("  Starting items: "))?
            .split(", ")
            .map(str::parse)
            .collect::<Result<_, _>>()?;
        Ok(Monkey {
            items,
            op: to_op(b(lines.next())?)?,
            test: int(&mut lines)?,
            t: int(&mut lines)?,
            f: int(&mut lines)?,
            examined: 0,
        })
    }
}

fn solve(monkeys: Vec<Monkey>, rounds: usize, is_p2: bool) -> usize {
    let p2 = is_p2.then(|| monkeys.iter().map(|m| m.test).product::<u64>());
    let ms = monkeys.into_iter().map(RefCell::new).collect::<Vec<_>>();
    for m in ms.iter().cycle().take(rounds * ms.len()) {
        let mut m = m.borrow_mut();
        m.examined += m.items.len();
        let mut targets = [ms[m.f].borrow_mut(), ms[m.t].borrow_mut()];
        let (test, op) = (m.test, m.op.clone());
        for wl in m.items.drain(..) {
            let wl = p2.map_or_else(|| op(wl) / 3, |m| op(wl) % m);
            targets[usize::from(wl % test == 0)].items.push(wl);
        }
    }
    let mut ms = ms.into_iter().map(RefCell::into_inner).collect::<Vec<_>>();
    ms.sort_unstable_by_key(|m| std::cmp::Reverse(m.examined));
    ms[0].examined * ms[1].examined
}

#[aoc(day11, part1, separator = "\n\n")]
fn part1(monkeys: Vec<Monkey>) -> usize {
    solve(monkeys, 20, false)
}

#[aoc(day11, part2, separator = "\n\n")]
fn part2(monkeys: Vec<Monkey>) -> usize {
    solve(monkeys, 10_000, true)
}
