use std::ops::RangeInclusive;

#[aoc(day4, part1)]
fn part1(input: &str) -> eyre::Result<usize> {
    Ok(parse(input)?
        .into_iter()
        .filter(|(a, b)| {
            (a.contains(b.start()) && a.contains(b.end()))
                || (b.contains(a.start()) && b.contains(a.end()))
        })
        .count())
}

#[aoc(day4, part2)]
fn part2(input: &str) -> eyre::Result<usize> {
    Ok(parse(input)?
        .into_iter()
        .filter(|(a, b)| b.end() >= a.start() && a.end() >= b.start())
        .count())
}

fn parse(input: &str) -> eyre::Result<Vec<(RangeInclusive<u32>, RangeInclusive<u32>)>> {
    input
        .lines()
        .map(|l| {
            let (a, b) = l.split_once(',').unwrap();
            let (ab, ae) = a.split_once('-').unwrap();
            let (bb, be) = b.split_once('-').unwrap();
            Ok((ab.parse()?..=ae.parse()?, bb.parse()?..=be.parse()?))
        })
        .collect()
}
