#[aoc(day1, part1)]
fn part1(input: &str) -> eyre::Result<u32> {
    Ok(parse(input)?.into_iter().max().unwrap())
}

#[aoc(day1, part2)]
fn part2(input: &str) -> eyre::Result<u32> {
    let mut v = parse(input)?;
    v.sort_unstable();
    Ok(v.into_iter().rev().take(3).sum())
}

fn parse(input: &str) -> Result<Vec<u32>, std::num::ParseIntError> {
    input
        .split("\n\n")
        .map(|s| s.lines().map(str::parse::<u32>).sum())
        .collect()
}
