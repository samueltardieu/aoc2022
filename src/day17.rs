use std::collections::{BTreeSet, HashMap};
use std::hash::Hash;

type Terrain = BTreeSet<(usize, usize)>;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
enum Piece {
    Horiz,
    Cross,
    Ell,
    Vert,
    Square,
}

impl Piece {
    fn order() -> impl Iterator<Item = Piece> {
        [
            Piece::Horiz,
            Piece::Cross,
            Piece::Ell,
            Piece::Vert,
            Piece::Square,
        ]
        .into_iter()
        .cycle()
    }

    fn solid(self) -> Vec<(usize, usize)> {
        match self {
            Piece::Horiz => vec![(0, 0), (1, 0), (2, 0), (3, 0)],
            Piece::Cross => vec![(1, 0), (0, 1), (1, 1), (2, 1), (1, 2)],
            Piece::Ell => vec![(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)],
            Piece::Vert => vec![(0, 0), (0, 1), (0, 2), (0, 3)],
            Piece::Square => vec![(0, 0), (1, 0), (0, 1), (1, 1)],
        }
    }

    fn parts(self, base: (usize, usize)) -> impl Iterator<Item = (usize, usize)> {
        self.solid()
            .into_iter()
            .map(move |d| (base.0 + d.0, base.1 + d.1))
    }

    fn fits(self, pos: (usize, usize), terrain: &Terrain) -> bool {
        self.parts(pos)
            .all(|p| p.0 >= 1 && p.0 <= 7 && p.1 >= 1 && !terrain.contains(&p))
    }
}

fn offseted(base: (usize, usize), offset: (isize, isize)) -> (usize, usize) {
    (
        ((base.0 as isize) + offset.0) as usize,
        ((base.1 as isize) + offset.1) as usize,
    )
}

fn solve(input: &str, n: usize) -> usize {
    let mut terrain = Terrain::default();
    let mut pieces = Piece::order();
    let commands = input.trim().as_bytes();
    let mut command_idx = 0usize;
    let mut cache = HashMap::<(usize, Piece, Terrain), (usize, usize)>::default();
    let mut heights = HashMap::<usize, usize>::default();
    let mut block = [false; 7];
    let mut high = 0;
    for i in 0..n {
        let piece = pieces.next().unwrap();
        if block.iter().all(|b| *b) {
            let mut found = [false; 7];
            for y in (1..=high).rev() {
                (0..7).for_each(|x| {
                    found[x] |= terrain.contains(&(x + 1, y));
                });
                if found.iter().all(|b| *b) {
                    terrain.retain(|(_, yy)| *yy + 3 >= y);
                    block.fill(false);
                    break;
                }
            }
        }
        let terrain_key = terrain
            .iter()
            .map(|(x, y)| (*x, high - *y))
            .collect::<BTreeSet<_>>();
        let key = (command_idx, piece, terrain_key);
        if let Some((old_i, old_high)) = cache.get(&key) {
            let idx = *old_i + ((n - i) % (i - old_i));
            let result = heights[&idx] + (n - idx) / (i - old_i) * (high - old_high);
            return result;
        }
        cache.insert(key, (i, high));
        heights.insert(i, high);
        let mut pos = (3, high + 4);
        loop {
            let command = commands[command_idx];
            command_idx = (command_idx + 1) % commands.len();
            let offset = if command == b'<' { (-1, 0) } else { (1, 0) };
            let moved = offseted(pos, offset);
            if piece.fits(moved, &terrain) {
                pos = moved;
            }
            let moved = offseted(pos, (0, -1));
            if piece.fits(moved, &terrain) {
                pos = moved;
            } else {
                for (x, y) in piece.parts(pos) {
                    block[x - 1] = true;
                    terrain.insert((x, y));
                }
                break;
            }
        }
        high = terrain.iter().map(|(_, y)| y).max().copied().unwrap();
    }
    high
}

#[aoc(day17, part1)]
fn part1(input: &str) -> usize {
    solve(input, 2022)
}

#[aoc(day17, part2)]
fn part2(input: &str) -> usize {
    solve(input, 1_000_000_000_000)
}
