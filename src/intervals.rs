#![allow(unused)]
//! Intervals

use std::{
    convert::identity,
    ops::{Add, Sub},
};

use num_traits::One;

/// A non-empty interval with a lower and upper bound
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Interval<T> {
    low: T,
    high: T,
}

impl<T> Interval<T> {
    /// Low bound
    pub fn low(&self) -> &T {
        &self.low
    }

    /// High bound
    pub fn high(&self) -> &T {
        &self.high
    }
}

impl<T> Interval<T>
where
    T: Ord,
{
    /// Create a new interval from the given low and high bounds, inclusive.
    ///
    /// # Panics
    ///
    /// This panics if the low bound is greater or equal than the high bound.
    pub fn new(low: T, high: T) -> Self {
        assert!(low <= high);
        Self { low, high }
    }

    /// Check if two intervals overlap.
    pub fn overlaps(&self, other: &Self) -> bool {
        other.low <= self.high && self.low <= other.high
    }

    /// Check if this interval contains another.
    pub fn contains(&self, other: &Self) -> bool {
        self.low <= other.low && self.high >= other.high
    }

    /// Check if this interval is contained in another.
    pub fn is_contained_in(&self, other: &Self) -> bool {
        other.contains(self)
    }

    /// Return an interval containing both intervals.
    #[must_use]
    pub fn enlarge(self, other: Self) -> Self {
        Self {
            low: self.low.min(other.low),
            high: self.high.max(other.high),
        }
    }

    /// Merge two overlapping intervals and return the result.
    /// Return `None` if the intervals do not overlap.
    #[must_use]
    pub fn merge_with(self, other: Self) -> Option<Self> {
        self.overlaps(&other).then(|| self.enlarge(other))
    }

    /// Intersect two intervals and return the result.
    /// Return `None` if the intersection is empty.
    #[must_use]
    pub fn intersect(self, other: Self) -> Option<Self> {
        let low = self.low.max(other.low);
        let high = self.high.min(other.high);
        (low < high).then(|| Self::new(low, high))
    }
}

impl<T: One + Ord + Add<Output = T> + Sub<Output = T> + Clone> Interval<T> {
    /// Split an interval in two at the given value,
    /// with the possibility to keep the value in both
    /// resulting intervals. The result is:
    /// - One interval if the value is removed and was
    ///   on one end, or if the value was the only value
    ///   and was kept.
    /// - Two intervals if the value is located inside
    ///   the interval.
    /// - Zero intervals if the value is located outside
    ///   the interval or was the sole value in the interval
    ///   and must be removed.
    ///
    /// # Examples
    ///
    /// ```
    /// use aoc2022::intervals::Interval;
    ///
    /// assert_eq!(vec![Interval::new(10, 16), Interval::new(16, 20)],
    ///            Interval::new(10, 20).split_at(16, true));
    /// assert_eq!(vec![Interval::new(10, 15), Interval::new(17, 20)],
    ///            Interval::new(10, 20).split_at(16, false));
    /// assert_eq!(vec![Interval::new(10, 10)],
    ///            Interval::new(10, 10).split_at(10, true));
    /// assert_eq!(Interval::new(10, 10).split_at(10, false),
    ///            vec![]);
    /// ```
    pub fn split_at(self, value: T, keep: bool) -> Vec<Self> {
        if value < self.low || value > self.high {
            vec![]
        } else if value == self.low {
            if keep {
                vec![self]
            } else if self.low < self.high {
                vec![Self {
                    low: self.low + T::one(),
                    high: self.high,
                }]
            } else {
                vec![]
            }
        } else if value == self.high {
            if keep {
                vec![self]
            } else if self.low < self.high {
                vec![Self {
                    low: self.low,
                    high: self.high - T::one(),
                }]
            } else {
                vec![]
            }
        } else if keep {
            vec![
                Self {
                    low: self.low,
                    high: value.clone(),
                },
                Self {
                    low: value,
                    high: self.high,
                },
            ]
        } else {
            vec![
                Self {
                    low: self.low,
                    high: value.clone() - T::one(),
                },
                Self {
                    low: value + T::one(),
                    high: self.high,
                },
            ]
        }
    }
}

impl<T: Ord + Clone> Interval<T> {
    /// Insert the interval in a sorted non-overlapping list of intervals.
    /// The interval will be merged with others if they overlap and the
    /// list will be kept sorted.
    pub fn insert_into(self, list: &mut Vec<Self>) {
        let mut pos = list
            .binary_search_by_key(&&self.low, |interval| &interval.low)
            .map_or_else(identity, identity);
        // Merge with previous interval if appropriate.
        if pos > 0 && list[pos - 1].high >= self.low {
            list[pos - 1].high = list[pos - 1].high.clone().max(self.high);
            pos -= 1;
        } else {
            list.insert(pos, self);
        }
        // Merge with next intervals while appropriate.
        while pos + 1 < list.len() && list[pos].high >= list[pos + 1].low {
            list[pos].high = list[pos].high.clone().max(list[pos + 1].high.clone());
            list.remove(pos + 1);
        }
    }

    /// Build a list of non-overlapping intervals from a list of intervals.
    ///
    /// # Example
    ///
    /// ```
    /// use aoc2022::intervals::Interval;
    ///
    /// let is = vec![Interval::new(10, 20), Interval::new(5, 15),
    ///     Interval::new(30, 40), Interval::new(12, 22)];
    /// let no = Interval::non_overlapping(is);
    /// assert_eq!(vec![Interval::new(5, 22), Interval::new(30, 40)], no);

    /// let is = vec![Interval::new(25, 30), Interval::new(10, 20), Interval::new(5, 15),
    ///     Interval::new(30, 40), Interval::new(4, 22), Interval::new(40, 41)];
    /// let no = Interval::non_overlapping(is);
    /// assert_eq!(vec![Interval::new(4, 22), Interval::new(25, 41)], no);
    /// ```
    #[must_use]
    pub fn non_overlapping(mut list: Vec<Self>) -> Vec<Self> {
        list.sort_unstable_by(|a, b| a.low.cmp(&b.low));
        let mut res: Vec<Self> = vec![];
        for interval in list {
            if let Some(current) = res.last_mut() {
                if current.high >= interval.low {
                    current.high = current.high.clone().max(interval.high);
                } else {
                    res.push(interval);
                }
            } else {
                res.push(interval);
            }
        }
        res
    }
}

impl<T: One + Add<Output = T> + Sub<Output = T> + Clone> Interval<T> {
    /// Return the lowest value greater than any value of the interval
    ///
    /// # Example
    ///
    /// ```
    /// use aoc2022::intervals::Interval;
    ///
    /// let i = Interval::new(10, 20);
    /// assert_eq!(21, i.after());
    /// ```
    pub fn after(&self) -> T {
        self.high.clone() + T::one()
    }

    /// Return the highest value lesser than any value of the interval
    ///
    /// # Example
    ///
    /// ```
    /// use aoc2022::intervals::Interval;
    ///
    /// let i = Interval::new(10, 20);
    /// assert_eq!(9, i.before());
    /// ```
    pub fn before(&self) -> T {
        self.low.clone() - T::one()
    }
}
