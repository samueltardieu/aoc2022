use super::FxHashSet;
use itertools::Itertools;
use nom::{
    bytes::complete::tag, character::complete as ch, multi::separated_list1,
    sequence::separated_pair, Finish, IResult,
};
use pathfinding::prelude::bfs_reach;

fn parse_line(input: &str) -> IResult<&str, Vec<(u32, u32)>> {
    separated_list1(tag(" -> "), separated_pair(ch::u32, ch::char(','), ch::u32))(input)
}

fn parse(input: &str) -> (FxHashSet<(u32, u32)>, u32) {
    let mut obstacles = FxHashSet::default();
    for l in input.lines().map(|l| parse_line(l).finish().unwrap().1) {
        for ((x1, y1), (x2, y2)) in l.into_iter().tuple_windows() {
            if x1 == x2 {
                obstacles.extend(((y1.min(y2))..=(y2.max(y1))).map(|y| (x1, y)));
            } else {
                obstacles.extend(((x1.min(x2))..=(x2.max(x1))).map(|x| (x, y1)));
            }
        }
    }
    let floor = *obstacles.iter().map(|(_, y)| y).max().unwrap();
    (obstacles, floor)
}

fn fall(obstacles: &mut FxHashSet<(u32, u32)>, floor: u32) -> usize {
    let (mut latest, mut count) = (vec![(500, 0)], 0);
    while let Some((mut x, mut y)) = latest.pop() {
        count += 1;
        'fall: while y < floor {
            for xx in [x, x - 1, x + 1] {
                if !obstacles.contains(&(xx, y + 1)) {
                    latest.push((x, y));
                    (x, y) = (xx, y + 1);
                    continue 'fall;
                }
            }
            if obstacles.insert((x, y)) {
                break;
            }
            return count; // Part 2
        }
        if y >= floor {
            return count - 1; // Part 1
        }
    }
    count
}

#[aoc(day14, part1)]
fn part1(input: &str) -> usize {
    let (mut obstacles, floor) = parse(input);
    fall(&mut obstacles, floor)
}

#[aoc(day14, part2)]
fn part2(input: &str) -> usize {
    let (mut obstacles, floor) = parse(input);
    obstacles.extend((0..1000).map(|x| (x, floor + 2)));
    fall(&mut obstacles, floor + 2)
}

#[aoc(day14, part2, bfs)]
fn part2_bfs(input: &str) -> usize {
    let (mut obstacles, floor) = parse(input);
    obstacles.extend((0..1000).map(|x| (x, floor + 2)));
    bfs_reach((500, 0), |&(x, y)| {
        [(x - 1, y + 1), (x, y + 1), (x + 1, y + 1)]
            .into_iter()
            .filter(|p| !obstacles.contains(p))
    })
    .count()
}
