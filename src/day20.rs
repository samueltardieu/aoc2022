use std::collections::VecDeque;

#[aoc(day20, part1)]
fn part1(values: &[i64]) -> i64 {
    solve(values, 1, 1)
}

#[aoc(day20, part2)]
fn part2(values: &[i64]) -> i64 {
    solve(values, 811_589_153, 10)
}

fn solve(values: &[i64], key: i64, rounds: usize) -> i64 {
    let mut values = values
        .iter()
        .map(|v| *v * key)
        .enumerate()
        .collect::<VecDeque<_>>();
    for _ in 0..rounds {
        for i in 0..values.len() {
            let idx = values.iter().position(|(j, _)| i == *j).unwrap();
            let (j, v) = values.remove(idx).unwrap();
            let d = (idx as i64 + v).rem_euclid(values.len() as i64) as usize;
            values.insert(d, (j, v));
        }
    }
    let idx = values.iter().position(|(_, v)| *v == 0).unwrap();
    values[(1000 + idx) % values.len()].1
        + values[(2000 + idx) % values.len()].1
        + values[(3000 + idx) % values.len()].1
}
