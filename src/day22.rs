use itertools::{iproduct, Itertools};
use regex::Regex;
use rustc_hash::FxHashMap;
use std::{convert::Infallible, str::FromStr};

type Point = (usize, usize);

#[derive(Debug)]
struct Maze {
    walls: FxHashMap<Point, bool>,
    row_bounds: Vec<Point>,
    col_bounds: Vec<Point>,
}

impl FromStr for Maze {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (mut walls, mut max_row, mut max_col) = (FxHashMap::default(), 0, 0);
        for (row, l) in s.lines().enumerate() {
            max_row = row.max(max_row);
            for (col, b) in l.bytes().enumerate() {
                max_col = col.max(max_col);
                match b {
                    b'#' => {
                        walls.insert((row, col), true);
                    }
                    b'.' => {
                        walls.insert((row, col), false);
                    }
                    _ => (),
                }
            }
        }
        let row_bounds = (0..=max_row)
            .map(|r| {
                walls
                    .keys()
                    .filter_map(|(rr, cc)| (*rr == r).then_some(*cc))
                    .minmax()
                    .into_option()
                    .unwrap()
            })
            .collect();
        let col_bounds = (0..=max_col)
            .map(|c| {
                walls
                    .keys()
                    .filter_map(|(rr, cc)| (*cc == c).then_some(*rr))
                    .minmax()
                    .into_option()
                    .unwrap()
            })
            .collect();
        Ok(Maze {
            walls,
            row_bounds,
            col_bounds,
        })
    }
}

#[derive(Debug)]
enum Instr {
    Forward(usize),
    Turn(bool),
} // Turn(true) is Right

fn parse_instrs(s: &str) -> Vec<Instr> {
    let re = Regex::new(r"(\d+)|[RDLU]").unwrap();
    re.captures_iter(s)
        .map(|c| match c[0].parse::<usize>() {
            Ok(n) => Instr::Forward(n),
            _ if &c[0] == "R" => Instr::Turn(true),
            _ => Instr::Turn(false),
        })
        .collect()
}

fn parse(input: &str) -> (Maze, Vec<Instr>) {
    let (m, i) = input.trim_end().split_once("\n\n").unwrap();
    (m.parse().unwrap(), parse_instrs(i))
}

#[aoc(day22, part1)]
fn part1(input: &str) -> usize {
    let (maze, instrs) = parse(input);
    let mut pos = iproduct!(0.., 0..)
        .find(|p| maze.walls.get(p) == Some(&false))
        .unwrap();
    let mut dir = (0isize, 1isize);
    for i in instrs {
        match i {
            Instr::Turn(true) => dir = (dir.1, -dir.0),
            Instr::Turn(false) => dir = (-dir.1, dir.0),
            Instr::Forward(n) => {
                for _ in 0..n {
                    let (c0a, c0b) = maze.row_bounds[pos.0];
                    let (r0a, r0b) = maze.col_bounds[pos.1];
                    let epos = (
                        ((pos.0 - r0a) as isize + dir.0).rem_euclid((r0b - r0a + 1) as isize)
                            as usize
                            + r0a,
                        ((pos.1 - c0a) as isize + dir.1).rem_euclid((c0b - c0a + 1) as isize)
                            as usize
                            + c0a,
                    );
                    if !maze.walls[&epos] {
                        pos = epos;
                    }
                }
            }
        }
    }
    (pos.0 + 1) * 1000 + (pos.1 + 1) * 4 + dir_value(dir)
}

#[aoc(day22, part2)]
fn part2(input: &str) -> usize {
    let (maze, instrs) = parse(input);
    let mut pos = iproduct!(0.., 0..)
        .find(|p| maze.walls.get(p) == Some(&false))
        .unwrap();
    let mut dir = (0isize, 1isize);
    for i in instrs {
        match i {
            Instr::Turn(true) => dir = (dir.1, -dir.0),
            Instr::Turn(false) => dir = (-dir.1, dir.0),
            Instr::Forward(n) => {
                for _ in 0..n {
                    let (epos, edir) = walk(&pos, &dir);
                    if !maze.walls[&epos] {
                        pos = epos;
                        dir = edir;
                    }
                }
            }
        }
    }
    (pos.0 + 1) * 1000 + (pos.1 + 1) * 4 + dir_value(dir)
}

macro_rules! cube {
    {
        cube size is $facesize:expr;
        $(side $side:literal is at ($row:expr, $col:expr);)*
        $(connect side $lside:literal $ldir:ident with side $rside:literal $rdir:ident);*
        $(;)?
     } => {
        const FACE_SIZE: usize = $facesize;

        fn pos_to_side(pos: &Point) -> usize {
            match pos {
                $(($row, $col) => $side,)*
                _ => panic!("{pos:?} does not correspond to a known top-left corner"),
            }
        }

        fn side_to_pos(side: usize) -> Point {
            match side {
                $($side => ($row, $col),)*
                _ => panic!("{side} is not a known side"),
            }
        }

        fn continuation(pos: &Point, dir: &Dir) -> (Point, Dir) {
            match (pos_to_side(pos), *dir) {
                $(
                        ($lside, $ldir) => (side_to_pos($rside), cube!(rev $rdir)),
                        ($rside, $rdir) => (side_to_pos($lside), cube!(rev $ldir)),
                )*
                (s, d) => panic!("don't know how to get in direction {d:?} from side {s}"),
            }
        }
     };
     (rev UP) => (DOWN);
     (rev DOWN) => (UP);
     (rev LEFT) => (RIGHT);
     (rev RIGHT) => (LEFT);
}

type Dir = (isize, isize);
const DOWN: Dir = (1, 0);
const UP: Dir = (-1, 0);
const LEFT: Dir = (0, -1);
const RIGHT: Dir = (0, 1);

fn dir_value(dir: Dir) -> usize {
    match dir {
        RIGHT => 0,
        DOWN => 1,
        LEFT => 2,
        UP => 3,
        _ => panic!("unknown dir {dir:?}"),
    }
}

fn walk(pos: &Point, dir: &Dir) -> (Point, Dir) {
    const LF: usize = FACE_SIZE - 1;
    let top_left = (pos.0 / FACE_SIZE, pos.1 / FACE_SIZE);
    let rel = (
        pos.0 - top_left.0 * FACE_SIZE,
        pos.1 - top_left.1 * FACE_SIZE,
    );
    let crosses = (rel.0 == 0 && dir == &UP)
        || (rel.0 == LF && dir == &DOWN)
        || (rel.1 == 0 && dir == &LEFT)
        || (rel.1 == LF && dir == &RIGHT);
    let (tl, d) = if crosses {
        continuation(&top_left, dir)
    } else {
        (top_left, *dir)
    };
    let new_rel = match (*dir, d) {
        (UP, LEFT) => (LF - rel.1, LF),
        (LEFT, UP) => (LF, LF - rel.0),
        (UP, RIGHT) => (rel.1, 0),
        (RIGHT, UP) => (LF, rel.0),
        (DOWN, LEFT) => (rel.1, LF),
        (LEFT, DOWN) => (0, rel.0),
        (DOWN, RIGHT) => (LF - rel.1, 0),
        (RIGHT, DOWN) => (0, LF - rel.0),
        (RIGHT, LEFT) => (LF - rel.0, LF),
        (LEFT, RIGHT) => (LF - rel.0, 0),
        (DOWN, UP) => (LF, LF - rel.1),
        (UP, DOWN) => (0, LF - rel.1),
        (x, y) if x == y => (
            (rel.0 + FACE_SIZE).wrapping_add_signed(dir.0) % FACE_SIZE,
            (rel.1 + FACE_SIZE).wrapping_add_signed(dir.1) % FACE_SIZE,
        ),
        _ => panic!("unknown change from dir {dir:?} to dir {d:?} (pos = {pos:?}, tl = {tl:?}"),
    };
    (
        (tl.0 * FACE_SIZE + new_rel.0, tl.1 * FACE_SIZE + new_rel.1),
        d,
    )
}

cube! {
    cube size is 50;
    side 1 is at (0, 1);
    side 2 is at (0, 2);
    side 3 is at (1, 1);
    side 4 is at (2, 0);
    side 5 is at (2, 1);
    side 6 is at (3, 0);
    connect side 1 DOWN with side 3 UP;
    connect side 1 UP with side 6 LEFT;
    connect side 1 RIGHT with side 2 LEFT;
    connect side 1 LEFT with side 4 LEFT;
    connect side 2 DOWN with side 3 RIGHT;
    connect side 2 UP with side 6 DOWN;
    connect side 2 RIGHT with side 5 RIGHT;
    connect side 3 DOWN with side 5 UP;
    connect side 3 LEFT with side 4 UP;
    connect side 4 DOWN with side 6 UP;
    connect side 4 RIGHT with side 5 LEFT;
    connect side 5 DOWN with side 6 RIGHT;
}

// Example
// cube! {
//     cube size is 4;
//     side 1 is at (0, 2);
//     side 2 is at (1, 0);
//     side 3 is at (1, 1);
//     side 4 is at (1, 2);
//     side 5 is at (2, 2);
//     side 6 is at (2, 3);
//     connect side 1 DOWN with side 4 UP;
//     connect side 1 UP with side 2 UP;
//     connect side 1 RIGHT with side 6 RIGHT;
//     connect side 1 LEFT with side 3 UP;
//     connect side 2 DOWN with side 5 DOWN;
//     connect side 2 RIGHT with side 3 LEFT;
//     connect side 2 LEFT with side 6 DOWN;
//     connect side 3 DOWN with side 5 LEFT;
//     connect side 3 RIGHT with side 4 LEFT;
//     connect side 4 DOWN with side 5 UP;
//     connect side 4 RIGHT with side 6 UP;
//     connect side 5 RIGHT with side 6 LEFT;
// }
