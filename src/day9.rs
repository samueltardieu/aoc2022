#[aoc(day9, part1)]
fn part1(input: &str) -> usize {
    solve::<2>(input)
}

#[aoc(day9, part2)]
fn part2(input: &str) -> usize {
    solve::<10>(input)
}

fn solve<const NK: usize>(input: &str) -> usize {
    std::collections::BTreeSet::<(i32, i32)>::from_iter(
        (1..NK).fold(
            std::iter::once((0, 0))
                .chain(
                    input
                        .lines()
                        .map(|l| {
                            let (d, n) = l.split_once(' ').unwrap();
                            (d.as_bytes()[0], n.parse::<usize>().unwrap())
                        })
                        .flat_map(|(d, n)| vec![d; n])
                        .scan((0, 0), |(x, y), d| {
                            match d {
                                b'R' => *x += 1,
                                b'U' => *y += 1,
                                b'L' => *x -= 1,
                                _ => *y -= 1,
                            };
                            Some((*x, *y))
                        }),
                )
                .collect(),
            |h: Vec<_>, _| {
                h.into_iter()
                    .scan((0i32, 0i32), |(tx, ty), (hx, hy)| {
                        if tx.abs_diff(hx).max(ty.abs_diff(hy)) > 1 {
                            *tx += (hx - *tx).signum();
                            *ty += (hy - *ty).signum();
                        };
                        Some((*tx, *ty))
                    })
                    .collect()
            },
        ),
    )
    .len()
}
