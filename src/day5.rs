use regex::Regex;

type Crates = Vec<Vec<char>>;
type Instrs = Vec<(usize, usize, usize)>;

fn parse(input: &str) -> eyre::Result<(Crates, Instrs)> {
    let (first, second) = input.split_once("\n\n").unwrap();
    let mut input = first.lines().collect::<Vec<_>>();
    input.pop();
    let ncrates = (input[0].len() + 1) / 4;
    let mut crates = vec![vec![]; ncrates];
    for l in input {
        for (i, s) in crates.iter_mut().enumerate() {
            let c = l.as_bytes()[1 + 4 * i];
            if c.is_ascii_uppercase() {
                s.push(c as char);
            }
        }
    }
    crates.iter_mut().for_each(|v| v.reverse());
    let re = Regex::new(r"^move (\d+) from (\d+) to (\d+)$")?;
    let instrs = second
        .lines()
        .map(|l| {
            let m = re.captures(l).unwrap();
            Ok((m[1].parse()?, m[2].parse()?, m[3].parse()?))
        })
        .collect::<Result<_, std::num::ParseIntError>>()?;
    Ok((crates, instrs))
}

#[aoc(day5, part1)]
fn part1(input: &str) -> eyre::Result<String> {
    let (mut crates, instr) = parse(input)?;
    for (n, f, t) in instr {
        for _ in 0..n {
            let top = crates[f - 1].pop().unwrap();
            crates[t - 1].push(top);
        }
    }
    Ok(to_str(&crates))
}

#[aoc(day5, part2)]
fn part2(input: &str) -> eyre::Result<String> {
    let (mut crates, instr) = parse(input)?;
    for (n, f, t) in instr {
        let split_point = crates[f - 1].len() - n;
        let mut top = crates[f - 1].split_off(split_point);
        crates[t - 1].append(&mut top);
    }
    Ok(to_str(&crates))
}

fn to_str(crates: &Crates) -> String {
    crates.iter().map(|c| c.last().copied().unwrap()).collect()
}
