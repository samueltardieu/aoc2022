use crate::FxHashMap;
use ndarray::Array3;
use pathfinding::prelude::dijkstra_all;
use std::num::NonZeroU16;

// Valve AA has flow rate=0; tunnels lead to valves QT, AP, EZ, AK, XV
fn parse(input: &str) -> FxHashMap<String, (u16, Vec<String>)> {
    let re = regex::Regex::new(r"Valve (.*) has flow rate=(\d+); tunnels? leads? to valves? (.*)")
        .unwrap();
    input
        .lines()
        .map(|l| {
            let m = re.captures(l).expect("cannot match {l}");
            (
                m[1].to_owned(),
                (
                    m[2].parse::<u16>().unwrap(),
                    m[3].split(", ").map(str::to_owned).collect::<Vec<_>>(),
                ),
            )
        })
        .collect()
}

fn compress(
    network: &FxHashMap<String, (u16, Vec<String>)>,
) -> FxHashMap<String, (u16, Vec<(String, u16)>)> {
    network
        .iter()
        .filter(|(n, (flow, _))| *flow != 0 || &**n == "AA")
        .map(|(start, (flow, _))| {
            let reachable = dijkstra_all(&&**start, |s| {
                network
                    .get(&**s)
                    .map(|(_, n)| n.iter().map(|n| (&**n, 1)))
                    .unwrap()
            });
            (
                start.clone(),
                (
                    *flow,
                    reachable
                        .into_iter()
                        .filter_map(|(n, (_, cost))| {
                            (network[n].0 != 0).then(|| (n.to_owned(), cost))
                        })
                        .collect::<Vec<_>>(),
                ),
            )
        })
        .collect()
}

#[allow(clippy::type_complexity)]
fn recode(
    network: &FxHashMap<String, (u16, Vec<(String, u16)>)>,
) -> (Vec<(u16, Vec<(usize, u16)>)>, Vec<String>) {
    let mut mapping = network.keys().cloned().collect::<Vec<String>>();
    mapping.sort_unstable();
    let rev = mapping
        .iter()
        .enumerate()
        .map(|(i, s)| (s, i))
        .collect::<FxHashMap<_, _>>();
    let mut network = mapping
        .iter()
        .map(|n| {
            let &(valve, ref ns) = &network[n];
            (
                valve,
                ns.iter().map(|(n, c)| (rev[&n], *c)).collect::<Vec<_>>(),
            )
        })
        .collect::<Vec<_>>();
    let valves = network.iter().map(|(v, _)| *v).collect::<Vec<_>>();
    for (_, ns) in &mut network {
        ns.sort_unstable_by_key(|(n, _)| std::cmp::Reverse(valves[*n]));
    }
    (network, mapping)
}

#[allow(clippy::too_many_arguments)]
#[allow(clippy::type_complexity)]
fn solve(
    pos: usize,
    mut remaining: u16,
    other: Option<NonZeroU16>,
    network: &Vec<(u16, Vec<(usize, u16)>)>,
    mut opened: usize,
    mut still_closed: u16,
    mut so_far: u16,
    mut max_so_far: u16,
) -> u16 {
    let max_possible = still_closed * ((remaining - 1) + (other.map_or(2, NonZeroU16::get) - 2));
    if so_far + max_possible < max_so_far {
        return so_far;
    }
    let flow = (pos != 0)
        .then(|| {
            opened |= 1 << pos;
            remaining -= 1;
            still_closed -= network[pos].0;
            network[pos].0 * remaining
        })
        .unwrap_or_default();
    so_far += flow;
    max_so_far = max_so_far.max(so_far);
    network[pos]
        .1
        .iter()
        .filter(|(n, c)| opened & (1 << n) == 0 && c + 2 <= remaining)
        .for_each(|(n, c)| {
            let r = solve(
                *n,
                remaining - c,
                other,
                network,
                opened,
                still_closed,
                so_far,
                max_so_far,
            );
            max_so_far = max_so_far.max(r);
        });
    if let Some(r) = other {
        if remaining <= r.get() / 2 {
            max_so_far = max_so_far.max(solve(
                0,
                r.get(),
                None,
                network,
                opened,
                still_closed,
                so_far,
                max_so_far,
            ));
        }
    }
    max_so_far
}

#[aoc(day16, part1)]
fn part1(input: &str) -> u16 {
    let (network, _) = recode(&compress(&parse(input)));
    let still_closed = network.iter().map(|(c, _)| c).sum::<u16>();
    solve(0, 30, None, &network, 0, still_closed, 0, 0)
}

#[aoc(day16, part2)]
fn part2(input: &str) -> u16 {
    let (network, _) = recode(&compress(&parse(input)));
    let still_closed = network.iter().map(|(c, _)| c).sum::<u16>();
    let other = Some(NonZeroU16::new(26).unwrap());
    solve(0, 26, other, &network, 0, still_closed, 0, 0)
}

// Inspired by https://www.reddit.com/r/adventofcode/comments/zn6k1l/2022_day_16_solutions/j0gmocd/
fn positions(network: &[(u16, Vec<(usize, u16)>)], max_time: usize) -> Array3<u16> {
    let n = network.len();
    let mut flows = Array3::zeros([max_time, n, 1 << n]);
    for time in 1..max_time {
        for pos in 0..n {
            for opened_valves in 0..(1 << n) {
                let mut f = if opened_valves & (1 << pos) == 0 {
                    0
                } else {
                    flows[(time - 1, pos, opened_valves & !(1 << pos))]
                        + network[pos].0 * time as u16
                };
                for (neighbour, dist) in &network[pos].1 {
                    if time as u16 >= *dist {
                        f = f.max(flows[(time - *dist as usize, *neighbour, opened_valves)]);
                    }
                }
                flows[(time, pos, opened_valves)] = f;
            }
        }
    }
    flows
}

#[aoc(day16, part1, matrix)]
fn part1_matrix(input: &str) -> u16 {
    let (network, _) = recode(&compress(&parse(input)));
    let flows = positions(&network, 30);
    flows[(29, 0, (1 << network.len()) - 1)]
}

#[aoc(day16, part2, matrix)]
fn part2_matrix(input: &str) -> u16 {
    let (network, _) = recode(&compress(&parse(input)));
    let flows = positions(&network, 26);
    let (mut r, mask) = (0, 1 << network.len());
    for h in 0..mask {
        r = r.max(flows[(25, 0, h)] + flows[(25, 0, (mask - 1) & !h)]);
    }
    r
}
