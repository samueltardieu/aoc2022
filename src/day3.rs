#[aoc(day3, part1)]
fn part1(input: &str) -> u32 {
    input
        .lines()
        .map(|s| {
            let (a, b) = s.split_at(s.len() / 2);
            score(to_bitmap(a) & to_bitmap(b))
        })
        .sum()
}

#[aoc(day3, part2)]
fn part2(input: &str) -> u32 {
    input
        .lines()
        .collect::<Vec<_>>()
        .chunks(3)
        .map(|group| score(to_bitmap(group[0]) & to_bitmap(group[1]) & to_bitmap(group[2])))
        .sum()
}

fn to_bitmap(s: &str) -> u64 {
    s.bytes().fold(0, |a, b| a | 1 << u64::from(b - b'A'))
}

fn score(x: u64) -> u32 {
    match (x as f32).log2() {
        x if x < 26. => x as u32 + 27,
        x => x as u32 - 31,
    }
}
