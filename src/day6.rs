#[aoc(day6, part1)]
fn part1(input: &[u8]) -> Option<usize> {
    find_header::<4>(input)
}

#[aoc(day6, part2)]
fn part2(input: &[u8]) -> Option<usize> {
    find_header::<14>(input)
}

fn find_header<const LEN: usize>(input: &[u8]) -> Option<usize> {
    let mut counter = [0u8; 256];
    input[..LEN - 1]
        .iter()
        .for_each(|&c| counter[c as usize] += 1);
    input.windows(LEN).enumerate().find_map(|(i, w)| {
        counter[w[LEN - 1] as usize] += 1;
        if counter[b'a' as usize..=b'z' as usize]
            .iter()
            .all(|&v| v <= 1)
        {
            return Some(i + LEN);
        }
        counter[w[0] as usize] -= 1;
        None
    })
}
