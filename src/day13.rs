use nom::{
    branch::alt, character::complete as ch, combinator::map, multi::separated_list0,
    sequence::delimited, Finish, IResult,
};
use std::cmp::Ordering;

#[derive(Clone, Eq)]
enum Packet {
    Int(u32),
    List(Vec<Packet>),
}

impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Packet::Int(l), Packet::Int(r)) => l.partial_cmp(r),
            (Packet::List(l), Packet::List(r)) => l.partial_cmp(r),
            (l, Packet::List(r)) => match &**r {
                [r, ..] if l != r => l.partial_cmp(r),
                _ => 1usize.partial_cmp(&r.len()),
            },
            (Packet::List(_), Packet::Int(_)) => other.partial_cmp(self).map(Ordering::reverse),
        }
    }
}

impl Ord for Packet {
    fn cmp(&self, other: &Self) -> Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl PartialEq for Packet {
    fn eq(&self, other: &Self) -> bool {
        self.cmp(other).is_eq()
    }
}

fn parse(input: &str) -> Vec<Packet> {
    fn parse(line: &str) -> IResult<&str, Packet> {
        alt((
            delimited(
                ch::char('['),
                map(separated_list0(ch::char(','), parse), Packet::List),
                ch::char(']'),
            ),
            map(ch::u32, Packet::Int),
        ))(line)
    }
    input
        .lines()
        .filter_map(|l| parse(l).finish().map(|x| x.1).ok())
        .collect()
}

#[aoc(day13, part1)]
fn part1(input: &str) -> usize {
    parse(input)
        .chunks(2)
        .enumerate()
        .filter_map(|(i, lr)| (lr[0] < lr[1]).then_some(i + 1))
        .sum()
}

#[aoc(day13, part2)]
fn part2(input: &str) -> usize {
    let mut packets = parse(input);
    let [two, six] = &*parse("[[2]]\n[[6]]") else { unreachable!() };
    packets.extend([two.clone(), six.clone()]);
    packets.sort_unstable();
    packets
        .iter()
        .enumerate()
        .filter_map(|(i, p)| (p == two || p == six).then_some(i + 1))
        .product()
}
