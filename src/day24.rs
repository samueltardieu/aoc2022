use num_complex::Complex;
use pathfinding::prelude::bfs;
use std::collections::HashSet;

type Pos = Complex<isize>;
type Blizzard = (Pos, Pos);

struct Game {
    blizzards: Vec<Blizzard>,
    cache: Vec<HashSet<Pos>>,
    dims: Pos,
}

impl Game {
    fn new(blizzards: Vec<Blizzard>, dims: Pos) -> Self {
        let cache = vec![blizzards.iter().map(|(p, _)| *p).collect()];
        Game {
            blizzards,
            cache,
            dims,
        }
    }

    fn blizzards_at(&mut self, step: usize) -> &HashSet<Pos> {
        while step >= self.cache.len() {
            self.cache.push(
                self.blizzards
                    .iter()
                    .map(|(pos, dir)| {
                        let pos = pos + dir * step as isize;
                        let x = (pos.re - 1).rem_euclid(self.dims.re - 2) + 1;
                        let y = (pos.im - 1).rem_euclid(self.dims.im - 2) + 1;
                        Complex::new(x, y)
                    })
                    .collect(),
            );
        }
        &self.cache[step]
    }

    fn is_playable(&mut self, pos: Pos, step: usize) -> bool {
        (1..=self.dims.re - 2).contains(&pos.re)
            && (1..=self.dims.im - 2).contains(&pos.im)
            && !self.blizzards_at(step).contains(&pos)
    }
}

fn parse(input: &str) -> Game {
    let blizzards = input
        .lines()
        .enumerate()
        .flat_map(|(y, l)| {
            l.bytes().enumerate().filter_map(move |(x, b)| {
                let p = Complex::new(x as isize, y as isize);
                match b {
                    b'>' => Some((p, Complex::new(1, 0))),
                    b'^' => Some((p, Complex::new(0, -1))),
                    b'<' => Some((p, Complex::new(-1, 0))),
                    b'v' => Some((p, Complex::new(0, 1))),
                    _ => None,
                }
            })
        })
        .collect();
    let dims = Complex::new(
        input.lines().next().unwrap().len() as isize,
        input.lines().count() as isize,
    );
    Game::new(blizzards, dims)
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
struct Play {
    pos: Pos,
    step: usize,
    round: usize,
}

#[aoc(day24, part1)]
fn part1(input: &str) -> usize {
    solve(input, 1)
}

#[aoc(day24, part2)]
fn part2(input: &str) -> usize {
    solve(input, 3)
}

fn solve(input: &str, rounds: usize) -> usize {
    let mut game = parse(input);
    let start = Complex::new(1, 0);
    let play = Play {
        pos: start,
        step: 0,
        round: 0,
    };
    let target = game.dims - Complex::new(2, 1);
    let goals = [target, start]
        .into_iter()
        .cycle()
        .take(rounds)
        .collect::<Vec<_>>();
    let p = bfs(
        &play,
        |n| {
            [
                Complex::new(1, 0),
                Complex::new(-1, 0),
                Complex::new(0, 1),
                Complex::new(0, -1),
                Complex::new(0, 0),
            ]
            .into_iter()
            .filter_map(|d| {
                let p = n.pos + d;
                let round =
                    n.round + usize::from(n.pos != goals[n.round] && p == goals[n.round]);
                (round < rounds && game.is_playable(p, n.step + 1) || p == start || p == target)
                    .then_some({
                        Play {
                            step: n.step + 1,
                            pos: p,
                            round,
                        }
                    })
            })
            .collect::<Vec<_>>()
        },
        |n| n.round == rounds && n.pos == target,
    )
    .unwrap();
    p.len() - 1
}
