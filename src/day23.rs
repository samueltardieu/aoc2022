use itertools::Itertools;
use num_complex::Complex;
use std::collections::{HashMap, HashSet};

type Point = Complex<isize>;

macro_rules! def_complexs {
    ($($($n:ident),*: $a:expr, $b:expr);*) => {
        $($(const $n: Point = Complex::new($a, $b);)*)*
    };
}

def_complexs!(E: 1, 0; N: 0, 1; W: -1, 0; S: 0, -1; NE, L: 1, 1; SE, R: 1, -1; NW: -1, 1; SW: -1, -1);
const AROUND: [Point; 8] = [N, S, E, W, NW, SW, NE, SE];

fn next(dir: Point) -> Point {
    match dir {
        N => S,
        S => W,
        W => E,
        _ => N,
    }
}

fn dirs_from(dir: Point) -> impl Iterator<Item = Point> {
    std::iter::once(dir).chain((0..3).scan(dir, |s, _| {
        *s = next(*s);
        Some(*s)
    }))
}

fn parse(input: &str) -> Vec<Point> {
    input
        .lines()
        .enumerate()
        .flat_map(|(y, l)| {
            l.bytes().enumerate().filter_map(move |(x, b)| {
                (b == b'#').then_some(Complex::new(x as isize, -(y as isize)))
            })
        })
        .collect()
}

fn decisions(elves: &[Point], dir: Point) -> Vec<Point> {
    let positions: HashSet<&Point> = HashSet::from_iter(elves);
    elves
        .iter()
        .map(|p| {
            if AROUND.into_iter().all(|d| !positions.contains(&(p + d))) {
                *p
            } else {
                dirs_from(dir)
                    .find_map(|d| {
                        [d, d * L, d * R]
                            .into_iter()
                            .all(|d| !positions.contains(&(p + d)))
                            .then(|| p + d)
                    })
                    .unwrap_or(*p)
            }
        })
        .collect()
}

fn round(elves: &mut [Point], dir: &mut Point) -> bool {
    let choices = decisions(elves, *dir);
    *dir = next(*dir);
    let mut count: HashMap<&Point, usize> = HashMap::new();
    for p in &choices {
        *count.entry(p).or_default() += 1;
    }
    let mut change = false;
    for (e, c) in elves.iter_mut().zip(&choices) {
        if count[c] == 1 && e != c {
            change = true;
            *e = *c;
        }
    }
    change
}

#[aoc(day23, part1)]
fn part1(input: &str) -> usize {
    let mut elves = parse(input);
    let mut dir = N;
    for _ in 0..10 {
        round(&mut elves, &mut dir);
    }
    let (min_x, max_x) = elves.iter().map(|p| p.re).minmax().into_option().unwrap();
    let (min_y, max_y) = elves.iter().map(|p| p.im).minmax().into_option().unwrap();
    ((max_x - min_x + 1) * (max_y - min_y + 1)) as usize - elves.len()
}

#[aoc(day23, part2)]
fn part2(input: &str) -> usize {
    let mut elves = parse(input);
    let mut dir = N;
    for i in 1.. {
        if !round(&mut elves, &mut dir) {
            return i;
        }
    }
    unreachable!();
}
